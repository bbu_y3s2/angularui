import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { AlertMessageService } from 'src/app/services/alert-message.service';
import { DepartmentService } from 'src/app/services/department.service';
declare const $:any;
@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {

  constructor(private departmentService: DepartmentService, private alertShow:AlertMessageService) { }

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  departmentList: any;
  departmentVisible: boolean = false;
  btnSave: boolean = true;
  btnUpdate: boolean = false;
  btnDelete: boolean = false;
  departmentId:any = 0;

  departmentForm = new FormGroup({
    departmentId: new FormControl(''),
    departmentNameKh: new FormControl(''),
    departmentNameEn: new FormControl(''),
    isActive: new FormControl(''),
    createdBy: new FormControl('0')
  });

  
  ngOnInit(): void {
    this.getDepartment();
  }
  ngOnDestroy():void{
    this.dtTrigger.unsubscribe();
  }

  getDepartment() {
    if($.fn.dataTable.isDataTable('#dtDepartment')){
      $('dtDepartment').DataTable().fnDestroy();
    }
    this.dtOptions = {
      pagingType: 'simple_numbers',
      searching: true,
      responsive: true,
      language:{
        searchPlaceholder: 'Search name'
      }
    }
    this.departmentService.getList().subscribe((res) => {
      if(res.status === "Success"){
        document.getElementById("btnClose")?.click();
        this.departmentList = res.department;
        this.dtTrigger.next(null);
        console.log(res);
      }
    });
  }

  buttonVisible(save: boolean, edit: boolean, remove: boolean) {
    if (save === true) {
      this.departmentVisible = true;
    } else {
      this.departmentVisible = false;
    }

    this.btnSave = save;
    this.btnUpdate = edit;
    this.btnDelete = remove;
  }

  ViewDepartment(departmentId: number, cud: number) {
    if (cud === 1) { //view
      this.buttonVisible(false, false, false);
    } else if (cud === 2) {//edit
      this.buttonVisible(false, true, false);
    } else if (cud === 3) {//delete
      this.buttonVisible(false, false, true);
    }

    this.departmentService.getOne(departmentId).subscribe((res) => {
      this.departmentForm = new FormGroup({
        departmentId: new FormControl(res.department[0].departmentId),
        departmentNameKh: new FormControl(res.department[0].departmentNameKh),
        departmentNameEn: new FormControl(res.department[0].departmentNameEn),
        isActive: new FormControl(res.department[0].isActive),
        createdBy: new FormControl(res.department[0].createdBy)
      });
    });
  };

  addDepartment() {
    this.buttonVisible(true, false, false)
  };

  cudDepartment(CUD: String){
    if(CUD === 'U'){
      this.departmentId = this.departmentForm.value.departmentId;
    }else if(CUD === 'D'){
      this.departmentId = this.departmentForm.value.departmentId;
      
    }
    let data={
      departmentId: this.departmentId,
      departmentNameKh: this.departmentForm.value.departmentNameKh,
      departmentNameEn: this.departmentForm.value.departmentNameKh,
      isActive: this.departmentForm.value.isActive,
      createBy: '0',
      lastUpdateBy: '0',
      cud: CUD
    }
    this.departmentService.CUDDepartment(data).subscribe((res)=>{
      if(res.status === "Success"){
        this.alertShow.alertMessage(res.status,res.message);
        this.getDepartment();
        console.log(res)
      }
    })
  }


  



}
