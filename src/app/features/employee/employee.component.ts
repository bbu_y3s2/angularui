import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { AddressService } from 'src/app/services/address.service';
import { AlertMessageService } from 'src/app/services/alert-message.service';
import { DepartmentService } from 'src/app/services/department.service';
import { EducationLevelService } from 'src/app/services/education-level.service';
import { EmployeeService } from 'src/app/services/employee.service';
import { MajorService } from 'src/app/services/major.service';
import { PositionService } from 'src/app/services/position.service';

declare const $:any;
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit{

  constructor(private departmentService: DepartmentService, 
    private positionService: PositionService,
    private addressService: AddressService,
    private employeeService:EmployeeService,
    private educationService:EducationLevelService,
    private majorService:MajorService,
    private alert: AlertMessageService){}

  dtOptions: DataTables.Settings = {}
  dtTrigger: Subject<any> = new Subject <any>();
  
  employeeVisible: boolean = false;
  btnSave: boolean = false;
  btnUpdate: boolean = false;
  btnDelete: boolean = false;
  
  employeeId:any;

  

  employeeList: any;
  departmentList: any;
  positionList: any;
  majorList: any;
  provinceList: any;
  districtList: any;
  communeList: any;
  villageList: any;
  experienceList: any= [];
  educationList: any=[];
  education:any;
  majorText: any = '';
  educationLevelText: any = '';
  employeeExperienceRequests: any=[];
  employeeEducationRequests: any = [];


  employeeForm = new FormGroup({
    employeeId : new FormControl(''),
    employeeNameKh : new FormControl(''),
    employeeNameEn : new FormControl(''),
    gender  : new FormControl(''),
    position : new FormControl(''),
    department : new FormControl(''),
    address : new FormControl(''),
    createBy : new FormControl('0'),
  });

  employeeExperience = new FormGroup({
    employeeExpId: new FormControl(0),
    position: new FormControl(''),
    salary: new FormControl('0'),
    dateJoin: new FormControl(''),
    dateResign: new FormControl(''),
  });

  employeeEducationForm = new FormGroup({
    employeeEduId: new FormControl(0),
    educationLevelId: new FormControl(0),
    educationLevelText: new FormControl(''),
    majorId: new FormControl(0),
    majorText: new FormControl(''),
    schoolName: new FormControl(''),
    yearStart: new FormControl(''),
    yearEnd: new FormControl('')
  });

  
  
             
  ngOnInit(): void{
    this.getEmployeeList();
    this.dtOptions ={
      pagingType: 'full_numbers',
      searching:true,
      pageLength: 4,
      responsive: true,
      language:{
        searchPlaceholder: 'Search Name'
      }
    }
    this.getDepartment();
    this.getPosition();
    this.getAddress('P','0');
    this.getMajor();
    this.getEducation();
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
}
  getEmployeeList(){
    if($.fn.dataTable.isDataTable('#dtEmployee')){
      $('#dtEmployee').dataTable().fnDestroy();
    }
    this.employeeService.getEmployee().subscribe((res)=>{
      if(res.status === "Success"){
        this.employeeList = res.emp;
        this.dtTrigger.next(null);
        console.log(res);
      }
    })
  }
  getDepartment(){
    this.departmentService.getList().subscribe((res)=>{
      if(res.status === "Success"){
        this.departmentList = res.department
        console.log(res);
      }
    })
  }

  getPosition(){
    this.positionService.getList().subscribe((res)=>{
      if(res.status === "Success"){
        this.positionList = res.position;
        console.log(res);
      }
    })
  }

  getAddress(PDCV:string, parentId: any){
    this.addressService.getAddress(parentId).subscribe((res)=>{
      if(res.status === "Success"){
        if(PDCV === 'P'){
          this.provinceList = res.address;
          console.log(res)
        }else if(PDCV === 'D'){
          this.districtList = res.address;
          console.log(res)
        }else if(PDCV === 'C'){
          this.communeList = res.address;
          console.log(res)
        }else if(PDCV === 'V'){
          this.villageList = res.address;
          console.log(res)
        }
      }
    });
  }
  
  getMajor(){
    this.majorService.getMajor().subscribe((res)=>{
      if(res.status == "Success"){
        this.majorList = res.major;
        console.log(res);
      }
    })
  }
  getEducation(){
    this.educationService.getEduLevel().subscribe((res)=>{
      if(res.status == "Success"){
        this.education = res.educationLevel;
        console.log(res)
      }
    })
  }

  onChangeProvince(event:any){
    let provinceId = event.target.selectedOptions[0].value;
    this.getAddress('D',provinceId);
  }

  onChangeDistrict(event:any){
    let districtId = event.target.selectedOptions[0].value;
    this.getAddress('C',districtId);
  }

  onChangeCommune(event:any){
    let communeId = event.target.selectedOptions[0].value;
    this.getAddress('V',communeId);
  }


  addExperience(){
    let recordExist = this.experienceList.filter((x:{position: any})=> x.position === this.employeeExperience.value.position);
    if(recordExist.length ===1){
      this.alert.alertMessage("Error","This position is existed...!!!");
    }else{
      this.experienceList.push(this.employeeExperience.value);
    }
  }
  removeExperience(position: any){
    let i = this.experienceList.findIndex((x: {position: any})=>x.position === position);
    this.experienceList.splice(i,1);
  }
  addEducation(){
    let recordExist = this.educationList.filter((x:{educationLevelId: any})=> x.educationLevelId === this.employeeEducationForm.value.educationLevelId);
    if(recordExist.length === 1){
      this.alert.alertMessage("Error","This education Existed! ")
    }else{
      this.employeeEducationForm.value.educationLevelText = this.educationLevelText;
      this.employeeEducationForm.value.majorText = this.majorText;
      this.educationList.push(this.employeeEducationForm.value);
    }
  }
  removeEducation(educationLevelID: any){
     let i = this.educationList.findIndex((x: {educationLevelId: any})=>x.educationLevelId === educationLevelID);
    this.educationList.splice(i,1);
  }
  onMajorChange(event: any){
    this.majorText = event.target.selectedOptions[0].text;
  }
  onEducationChange(event: any){
    this.educationLevelText = event.target.selectedOptions[0].text;
  }

  saveEmployee(){
    let emp = this.employeeForm.value;
    let data: any =
    {
      'employeeId': emp.employeeId,
       "employeeNameKh": emp.employeeNameKh,
      "employeeNameEn": emp.employeeNameEn,
      "gender": emp.gender,
      "position": emp.position,
      "department": emp.department,
      "address": emp.address,
      "createBy": emp.createBy,
      "employeeExpRequests": this.experienceList,
      "employeeEduRequests": this.educationList,
    }
    console.log(emp);
    this.employeeService.addEmployee(data).subscribe((res)=>{
      if(res.status === "Success"){
        this.getEmployeeList();
        document.getElementById('btnClose')?.click();
      }
      this.alert.alertMessage(res.status,res.message);
    });
  }

  buttonVisible(save:boolean, edit:boolean, remove:boolean){
    if(save === true){
      this.employeeVisible = true;
    }else{
      this.employeeVisible = false;
    }
    this.btnSave = save;
    this.btnUpdate = edit;
    this.btnDelete = remove;
  }

  ViewEmployee(employeeId:string, CUD:number){
    if(CUD === 1){
      this.buttonVisible(false,false,false);
    }else if(CUD === 2){
      this.buttonVisible(false,true,false);
    }else if(CUD === 3){
      this.buttonVisible(false,false,true);
    }
    this.employeeService.getOne(employeeId).subscribe((res)=>{
      if(res.status === "Success"){
        this.employeeForm = new FormGroup({
          employeeId : new FormControl(''),
          employeeNameKh : new FormControl(''),
          employeeNameEn : new FormControl(''),
          gender  : new FormControl(''),
          position : new FormControl(''),
          department : new FormControl(''),
          address : new FormControl(''),
          createBy : new FormControl('0'),
        });
      }
    })
  }

  addEmployee(){
    this.buttonVisible(true,false,false);
  }

  CUDEmployee(CUD: string){
    if(CUD === 'U'){
      this.employeeId = this.employeeForm.value.employeeId;
    }else if(CUD === 'D'){
      this.employeeId = this.employeeForm.value.employeeId;
    }
    let emp = this.employeeForm.value;
    let data: any =
    {
      'employeeId': emp.employeeId,
      "employeeNameKh": emp.employeeNameKh,
      "employeeNameEn": emp.employeeNameEn,
      "gender": emp.gender,
      "position": emp.position,
      "department": emp.department,
      "address": emp.address,
      "createBy": emp.createBy,
      "employeeExpRequests": this.experienceList,
      "employeeEduRequests": this.educationList,
    }
    this.employeeService.cudEmployee(data).subscribe((res)=>{
      if(res.status === "Success"){
        this.getEmployeeList();
        console.log(res);
      }
    })
  }
  
};
