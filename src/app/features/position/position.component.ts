import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { AlertMessageService } from 'src/app/services/alert-message.service';
import { PositionService } from 'src/app/services/position.service';
declare const $:any;
@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.css']
})
export class PositionComponent implements OnInit,OnDestroy{

  constructor(private positionService:PositionService,private alertShow:AlertMessageService){}
  
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  positionList:any;
  positionVisible: boolean = false;
  btnSave: boolean = false;
  btnUpdate: boolean = false;
  btnDelete: boolean = false;
  positionId: any = 0;

  positionForm = new FormGroup({
    positionId: new FormControl(),
    positionNameKh: new FormControl(''), 
    positionNameEn: new FormControl(''), 
    isActive: new FormControl(''), 
    createBy: new FormControl('0'), 
    lastUpdateBy: new FormControl('0')
  });
  
  
  ngOnInit(): void {
    this.getPosition();
  }
  ngOnDestroy(): void {
      this.dtTrigger.unsubscribe();
  }
  getPosition(){
    if($.fn.dataTable.isDataTable('#dtPosition')){
      $('#dtPosition').dataTable().fnDestroy();
    }
    this.dtOptions ={
      pagingType: 'simple_numbers',
      searching: true,
      responsive: true,
      language:{
        searchPlaceholder: 'Search name'
      }
    };
    this.positionService.getList().subscribe((res)=>{
      if(res.status === "Success"){
        document.getElementById("btnClose")?.click();
        this.positionList = res.position;
        this.dtTrigger.next(null);
      }
    })
  }
  
  buttonVisible(save:boolean, edit:boolean, remove:boolean){
    if(save === true){
      this.positionVisible = true;
    }else{
      this.positionVisible = false;
    }
    this.btnSave = save;
    this.btnUpdate = edit;
    this.btnDelete = remove;
  }

  ViewPosition(positionId:number, cud:number){
    if(cud === 1){
      this.buttonVisible(false,false,false);
    }else if(cud === 2){
      this.buttonVisible(false,true,false);
    }else if(cud === 3){
      this.buttonVisible(false,false,true);
    }
    this.positionService.getOne(positionId).subscribe((res)=>{
      if(res.status === "Success"){
        this.positionForm = new FormGroup({
          positionId: new FormControl(res.position[0].positionId),
          positionNameKh: new FormControl(res.position[0].positionNameKh),
          positionNameEn: new FormControl(res.position[0].positionNameEn),
          isActive: new FormControl(res.position[0].isActive),
          createBy: new FormControl('0'),
          lastUpdateBy: new FormControl('0')
        })
      }
    })
  }

  AddPosition(){
    this.buttonVisible(true,false,false);
  }

  CUDPosition(CUD: String){
    if(CUD === 'U'){
      this.positionId = this.positionForm.value.positionId;
    }else if(CUD === 'D'){
      this.positionId = this.positionForm.value.positionId;
      
    }
    let data={
      positionId: this.positionId,
      positionNameKh: this.positionForm.value.positionNameKh,
      positionNameEn: this.positionForm.value.positionNameEn,
      isActive: this.positionForm.value.isActive,
      createBy: '0',
      lastUpdateBy: '0',
      cud: CUD
    }
    this.positionService.cudPosition(data).subscribe((res)=>{
      if(res.status === "Success"){
        this.alertShow.alertMessage(res.status,res.message);
        this.getPosition();
        console.log()
      }
    })
  }

}
