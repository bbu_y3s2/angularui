import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }
  
  getEmployee(){
    return this.http.get<any>('http://localhost:5211/api/getEmployeeList');
  }
  getOne(employeeId:string){
    return this.http.get<any>('http://localhost:5211/api/getEmployeeById'+employeeId);
  }
  addEmployee(data:any){
    return this.http.post<any>('http://localhost:5211/api/addEmployee',data);
  }
  cudEmployee(data:any){
    return this.http.post<any>('http://localhost:5211/api/CUDEmployee',data)
  }
}
