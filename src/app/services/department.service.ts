import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  constructor(private http: HttpClient) {}
  
  getList(){
    return this.http.get<any>('http://localhost:5211/api/departmentList');
  }
  getOne(departmentId:number){
    return this.http.get<any>('http://localhost:5211/api/getDepartmentById/'+departmentId)
  }
  CUDDepartment(data:any){
    return this.http.post<any>('http://localhost:5211/api/CUDDepartment',data);
  }
  
}
