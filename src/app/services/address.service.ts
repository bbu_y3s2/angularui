import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private http:HttpClient) { }

  getAddress(parentId:any){
    let data = {
      id: "",
      nameKh:"",
      nameEn:"",
      parentId : parentId
    };
    return this.http.post<any>('http://localhost:5211/api/getAddress',data);
  }
}
