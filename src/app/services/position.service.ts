import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PositionService {

  constructor(private http: HttpClient) { }

  getList(){
    return this.http.get<any>('http://localhost:5211/api/positionList');
  }

  getOne(positionId:number){
    return this.http.get<any>('http://localhost:5211/api/getPositionById/'+positionId);
  }

  cudPosition(data:any){
    return this.http.post<any>('http://localhost:5211/api/position-cud',data);
  }

}
