import { Injectable } from '@angular/core';
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class AlertMessageService {

  constructor(private toast: ToastrService) { }

  alertMessage(status: string, message: string){
    if(status === "Success"){
      this.toast.success(message,"Message");
    }else if(status === "Error"){
      this.toast.error(message,"Message");
    }else{
      this.toast.warning(message,"Message");
    }
  }
}
