import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EducationLevelService {

  constructor(private http:HttpClient) { }

  getEduLevel(){
    return this.http.get<any>('http://localhost:5211/api/getEducationLevel');
  }
}
