import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TestService {
  
  private apiUrl = "http://localhost:5211/api/Department";

  constructor(private http: HttpClient) { }

  getList(){
    return this.http.get('http://localhost:5211/api/Department/GetList');
  }
}
