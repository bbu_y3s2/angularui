FROM node:18.10 as build-step
RUN mkdir -p/app
WORKDIR /app
COPY package.json /app
RUN npm cache clean --force
RUN npm install
RUN npm build


FROM nginx:1.20.1

COPY nginx-config.conf /etc/nginx/conf.d/default.conf

EXPOSE 80